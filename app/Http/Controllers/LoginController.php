<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LoginController extends Controller
{
    // Lógica de autenticação para login de aluno
    public function loginAluno($usuario, $senha)
    {

        if ($usuario == 'anaclara123' && $senha == 'ana123') {
            return 'Bem-vindo, aluno!';
        } else {
            return 'Usuário ou senha inválidos.';
        }
    }

    public function loginFuncionario($usuario, $senha)
    {
        // Lógica de autenticação para login de funcionário
        if ($usuario == 'funcionario' && $senha == '123456') {
            return 'Bem-vindo, funcionário!';
        } else {
            return 'Usuário ou senha inválidos.';
        }
    }

    public function loginProfessor($usuario, $senha)
    {
        // Lógica de autenticação para login de professor
        if ($usuario == 'professor' && $senha == '123456') {
            return 'Bem-vindo, professor!';
        } else {
            return 'Usuário ou senha inválidos.';
        }
    }

    public function error()
    {
        return 'Erro! Página não encontrada.';
    }
}
