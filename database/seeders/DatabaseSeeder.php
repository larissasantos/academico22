<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Pessoa::factory()->create([
            'nome' => 'Aluno Teste',
            'tipo' => 'A',
        ]);

        \App\Models\Pessoa::factory()->create([
            'nome' => 'Professor Teste',
            'tipo' => 'P',
        ]);
        \App\Models\Pessoa::factory()->create([
            'nome' => 'Funcionario Teste',
            'tipo' => 'F',
        ]);
        \App\Models\Pessoa::factory(2)->create();
    }
}
