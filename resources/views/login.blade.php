 @extends('layouts.base')

 @section('conteudo')
  <div class="container flex ">
    <h5 class="row justify-content-center">LOGIN</h5>
    <div class="row justify-content-center">
      <img src="{{ asset('imgs/usuario.png')}}" id="imgUser" />
    </div>
    <form action="{{ route('register') }}" method="post" id="formLogin">
      @csrf
      <div class="form-group">
        <label for="usuario">Usuário:</label>
        <input type="text" class="form-control" id="usuario" name="usuario" placeholder="Nome do Usuário">
      </div>
      <div class="form-group">
        <label for="senha">Senha:</label>
        <input type="password" class="form-control" id="senha" name="senha" placeholder="Senha do usuário">
      </div>
      <button type="submit" class="btn btn-primary">Logar</button>
    </form>
  </div>
  @endsection
  